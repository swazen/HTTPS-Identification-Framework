#!/usr/lib/python

# Multi-Level identification Framework to Identify HTTPS Services
# Author by Wazen Shbair,
# University of Lorraine,
# France
# wazen.shbair@gmail.com
# January, 2017

####################################################
from functions import *
from get_statistics import *

print "Multi-Level Framework to Identify HTTPS Services"
print "------------------------------------------------"
print "[+] Phase #1: Building Training Dataset"
print "*************************************"
print "[+] Step 1: Enter your PCAP Folder name or Enter 00 to used an existing dataset: "
filename = raw_input()
if int(filename) !=0:
    print "[+] Step 2: TCP sessions Reassembly"
    cwd = os.getcwd()
    pkt2flow(cwd+"/"+filename)
    print "[+] Step 3: Features Extraction and Calculation"
    print " [-] Extract features from TCP flows"
    getFeatures(cwd+"/pkt2flow.out/tcp_syn/") # Give streams folder extracted from the pkt2flow library
    print " [-] Finished Extraction"
    print "[+] Step 4: Building Training Dataset (min. 50 connections per service)"
    BuildDataSet("streams_statisics.csv","trainingdataset.csv",40)
    print " [-] Training dataset is ready"


if int(filename)==0:
    print "[+] You have selected to use an existing dataset"

print "[+] Phase #2: Model Building and Validation"
print "----------------------------------------------------"
print "[+] Multi-level classification approach "
MultiLevelClassification("trainingdataset.csv",42)
print "----------------------------------------------------"
print "[+] Flat-level classification approach "+str(round((FlatClassification("trainingdataset.csv")),2))
print "----------------------------------------------------"