#!/usr/lib/python
# Auther Wazen Shbair
####################################################


import pyshark
import sys
import math
from datetime import *
from array import *
import numpy as np
from collections import Counter
from subprocess import Popen
import datetime
import time
from os import listdir
from os.path import isfile, join
import os
from collections import defaultdict
import re
import tldextract
import csv
from functions import *
from sklearn.ensemble import RandomForestClassifier
from collections import defaultdict
import pickle
from sklearn.externals import joblib
from sklearn import cross_validation
from sklearn.metrics import accuracy_score
from sklearn.ensemble import ExtraTreesClassifier
from sklearn import svm
import subprocess

################################################################
# Pkt2flow function to extract TCP flows from a given Pcap file
################################################################
def pkt2flow (pcapfile):
    cwd = os.getcwd()
    p = subprocess.Popen([cwd+"/pkt2flow/./pkt2flow", pcapfile], stdout=subprocess.PIPE)
    output, err = p.communicate()
    print " [-] TCP flow reassembly completed Successfully"

#########################################################################
#Function extract features to build a Training dataset
#########################################################################
def BuildDataSet(Streamsfile, outputfile, reso):
    filterstream = []
    head = "Tnum,Avsize,PsizeQ1,PsizeQ2,PsizeQ3,Var,Max,IATQ1,IATQ2,IATQ3,CSTnum,CSAvsize,CSPsizeQ1,CSPsizeQ2,CSPsizeQ3,CSVar,CSMax,CSIATQ1,CSIATQ2,CSIATQ3,SCTnum,SCAvsize,SCPsizeQ1,SCPsizeQ2,SCPsizeQ3,SCsize_var,SCMax,SCIATQ1,SCIATQ2,SCIATQ3,CSAppMean,CSAPP_Q1,CSAPP_Q2,CSAPP_Q3,CSAPP_var,CSAPP_max,SCAppMean,SCAPP_Q1,SCAPP_Q2,SCAPP_Q3,SCAPP_var,SCAPP_max,class,class2,orgnial" + "\n"
    f1 = open(outputfile, 'w')
    f1.write(head)
    file1 = open(Streamsfile)
    reader1 = csv.reader(file1, delimiter=',')

    file2 = open(Streamsfile)
    reader2 = csv.reader(file2, delimiter=',')

    snitmp = []
    for row in reader1:
        if row[0] not in filterstream:
            snitmp.append(SNIModificationbyone(row[0]))

    # Get flows with more than "reso" nummber
    uniqueServices = np.unique(snitmp)
    print "[+] Number of services considered : "+ str(len(uniqueServices))+ "services"
    for x in uniqueServices:
        c = snitmp.count(x)
        if c < reso and x not in filterstream:
            filterstream.append(x)

    for row in reader2:
        if SNIModificationbyone(row[0]) not in filterstream:
            temp = tldextract.extract(row[0])  #This is to extract the root Domain
            newsni = temp.domain + "." + temp.suffix

            f1.write(",".join(row[1:])+","+ SNIModificationbyone(row[0]) + ',' + newsni + ',' + temp.domain + "\n")

    f1.close()



#***********************************************************************************
#SNi modification for the sub-domain parts only
#***********************************************************************************
def SNIModificationbyone(sni):
    temp = tldextract.extract(sni)
    x = re.sub("\d+", "", temp.subdomain)  # remove numbers
    x = re.sub("[-,.]", "", x)  #remove dashes
    if len(x) > 0:
        newsni = x + "." + temp.domain + "." + temp.suffix  # reconstruct the sni
    else:
        newsni = temp.domain + "." + temp.suffix
    #print(x)


    return newsni


##########################################################
# Read/write csv files
##########################################################
def read_csv(file_path, has_header=True):
    with open(file_path) as f:
        if has_header: f.readline()
        data = []
        for line in f:
            line = line.strip().split(",")
            data.append([x for x in line])
    return data

def write_csv(file_path, data):
    with open(file_path, "w") as f:
        for line in data: f.write(",".join(line) + "\n")

####################################################
# Second Level models creator
####################################################
def Lv2modelsCreator(trainset, keyset, flimit):
    streamdict = defaultdict(list)
    models = defaultdict(list)
    dataset = zip(keyset, trainset)
    for row in dataset:
        streamdict[row[0]].append(row[1:44])
        randomforestmodel = RandomForestClassifier(n_estimators=10, n_jobs=10)

    for k in streamdict:
        perTLD = streamdict[k]
        #Full Feature set
        ktarget = np.array([x[0][flimit] for x in perTLD])
        ktrain = np.array([x[0][0:flimit] for x in perTLD])
        randomforestmodel.fit(ktrain, ktarget)
        s = pickle.dumps(randomforestmodel)
        models[k].append(s)
    return models

#############################################
# The Evaluation method
###############################################
def multiLevelEval(l2real, l2predict):
    total = 0
    partial = 0
    unknown = 0

    for i in range(0, len(l2real)):
        temp1 = tldextract.extract(l2real[i])
        temp2 = tldextract.extract(l2predict[i])
        realRD = temp1.domain + "." + temp1.suffix
        predictRD = temp2.domain + "." + temp2.suffix

        if l2real[i] == l2predict[i]:
            total=total+1.0

        elif realRD == predictRD:
            partial = partial + 1.0
    else:
        unknown = unknown + 1
    if realRD == "google.com" and predictRD == "gstatic.com":
        #partial=partial+1.0
        total = total + 1.0
    elif predictRD == "google.com" and realRD == "gstatic.com":
        #partial=partial+1.0
        total = total + 1.0

    print "[+] Partial : " + str(round(float(partial) / len(l2real),2))
    print "[+] Full :" + str(round((float(total) / len(l2real)),2))
    print "[+] Invalid :" + str(round((float(unknown) / len(l2real)),2))
    return (float(total) / len(l2real))


#############################################
# Multi-Level Classification 
#############################################
def MultiLevelClassification(datasetfile, flimit):
    dataset=read_csv(datasetfile)
    result = []
    X = np.array([z[0:43] for z in dataset])
    y = np.array([z[43] for z in dataset])

    rf = RandomForestClassifier(n_estimators=250, n_jobs=10)
    kf = cross_validation.StratifiedKFold(y, n_folds=10, indices=None)
    totalac = []
    for train_index, test_index in kf:
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        features = np.array([zp[0:flimit] for zp in X_train])
        features_test = np.array([zp[0:flimit] for zp in X_test])
        l2label = np.array([zp[flimit] for zp in X_test])
        Modles = Lv2modelsCreator(X_train, y_train, flimit)

        l2predict = []
        l2real = []

        streamdict = defaultdict(list)
        rf.fit(features, y_train)
        l1 = rf.predict(features_test)

        for i in range(0, len(l1)):
            streamdict[l1[i]].append(X_test[i])
        for k in streamdict:
            preTLD = streamdict[k]
            m = pickle.loads(Modles[k][0])
            feature = np.array([x[0:flimit] for x in preTLD])
            labels = np.array([x[flimit] for x in preTLD])
            l2 = m.predict(feature)
            l2predict = l2predict + l2.tolist()
            l2real = l2real + labels.tolist()
        totalac.append(multiLevelEval(l2real, l2predict))
        break

    return np.mean(totalac)

#############################################
# Flat Classification method
#############################################
def FlatClassification(datasetfile):
    dataset = read_csv(datasetfile)
    X = np.array([z[0:42] for z in dataset])
    y = np.array([z[42] for z in dataset])
    rf = RandomForestClassifier(n_estimators=250, n_jobs=10)
    kf = cross_validation.StratifiedKFold(y, n_folds=10, indices=None)

    for train_index, test_index in kf:
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        rf.fit(X_train, y_train)
        l1 = rf.predict(X_test)
        return accuracy_score(l1,y_test)