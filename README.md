              _    _ _______ _______ _____   _____ 
             | |  | |__   __|__   __|  __ \ / ____|
             | |__| |  | |     | |  | |__) | (___  
             |  __  |  | |     | |  |  ___/ \___ \ 
             | |  | |  | |     | |  | |     ____) |
             |_|  |_|  |_|     |_|  |_|    |_____/ 
                                       
          A Multi-level HTTPS Identification Framework
                
Overview:

We present a multi-level HTTPS identification framework, which includes machine 
learning techniques and a multi-level classification approach to identify the 
accessed HTTPS services in a given HTTPS flow without decryption.

Credits :

   This framework was developed by Wazen Shbair, Thibault Cholez, 
   Jérôme François and Isabelle Chrisment of MADYNES research
   group in LORIA, France. 
   
Contact:

    Thibault Cholez:  thibault.cholez@loria.fr
    Wazen Shbair:  wazen.shbair@gmail.com 

Related documnet:

	Wazen Shbair, Thibault Cholez, Jerome François, and Isabelle Chrisment, A
    multi-level framework to identify HTTPS services, The 28th IEEE/IFIP Net-
    work Operations and Management Symposium (NOMS2016), Istanbul, Turkey.
    (https://hal.inria.fr/hal-01273160)
 
Citation:

@inproceedings{shbair:hal-01273160,
  TITLE = {{A Multi-Level Framework to Identify HTTPS Services}},
  AUTHOR = {Shbair, Wazen M. and Cholez, Thibault and François Jerome and Chrisment, Isabelle},
  BOOKTITLE = {{IEEE/IFIP Network Operations and Management Symposium (NOMS 2016)}},
  ADDRESS = {Istanbul, Turkey},
  ORGANIZATION = {{IEEE/IFIP}},
  PUBLISHER = {{IEEE}},
  PAGES = {p240-248},
  YEAR = {2016},
  MONTH = Apr,
  DOI = {10.1109/NOMS.2016.7502818},
  KEYWORDS = {HTTPS ;  monitoring ;  traffic analysis ;  fingerprinting ;  machine learning},
  PDF = {https://hal.inria.fr/hal-01273160/file/HTTPS_identification_framework_slides.pdf},
  HAL_ID = {hal-01273160},
  HAL_VERSION = {v1},
}