#!/usr/lib/python
# This code extact the packet size and Inter-ARRIVAl time from the trace directly
#These is an option to get calclation based on direction client<>server or clien->server or server->client
#The code run in my labtop but on the machine not work, why ??? 

####################################
'''
Update list
1. Extreact the application data from the Application Data packet 
2. Calculate all the statistics of this data mean, number , Q1,Q2,Q3, VAR, AND MAX
   I THINK this data will be more valuable than the total packet size
2/9/2014 
The condition for file size is removed.
17/11/2014
Add dates to temp files to avoid conflict between running process
18/11/2014
1.Application data direction error, so now if no data in one direction we but 0
2.client to server error are corrected
3.all number are cieled 
'''
####################################
#Auther Wazen Shbair
import pyshark
import sys
import math
from datetime import *
from array import *
import numpy
from collections import Counter
from subprocess import Popen
import datetime
import time
from os import listdir
from os.path import isfile, join
import os


def getFeatures(foldername):
    path = foldername
    outputfile = "streams_statisics.csv"

    filelist = [f for f in listdir(path) if isfile(join(path, f)) if f.endswith(".pcap")]

    #Outputfile header 
    mainfile = open(outputfile, "w")
    mainfile.write(
        "class,Tnum,Avsize,PsizeQ1,PsizeQ2,PsizeQ3,Var,Max,IATQ1,IATQ2,IATQ3,CSTnum,CSAvsize,CSPsizeQ1,CSPsizeQ2,CSPsizeQ3,CSVar," +
        "CSMax,CSIATQ1,CSIATQ2,CSIATQ3,SCTnum,SCAvsize,SCPsizeQ1,SCPsizeQ2,SCPsizeQ3,SCsize_var,SCMax,SCIATQ1,SCIATQ2,SCIATQ3,CSAppMean,CSAPP_Q1,CSAPP_Q2,CSAPP_Q3,CSAPP_var,CSAPP_max,SCAppMean,SCAPP_Q1,SCAPP_Q2,SCAPP_Q3,SCAPP_var,SCAPP_max" + "\n")
    mainfile.close()
    #Get the number of files 
    filesnum = len(filelist)
    ipsource = ""

    for i in range(0, filesnum):

        filename = filelist[i]
        clientsni = ""
        count=0

        mincapture = pyshark.FileCapture(path + filename)
        mincapture.display_filter="ssl"

        x = pyshark.FileCapture(path + filename,
                                display_filter="ssl.handshake.type == 1 and ssl.handshake.extensions_server_name_len >0")
        appdatalength = pyshark.FileCapture(path + filename, display_filter="ssl.app_data")

        y = list(x)
        z = list(appdatalength)
        appdatalen = len(z)

        if len(y) > 0:
            clientsni = x[0].ssl.handshake_extensions_server_name
            ipsource = x[0].ip.src
        if appdatalen > 0 and clientsni != "":
            file2 = []
            file3 = []
            file4 = []
            file5 = []
            file6 = []
            file7 = []
            file18 = []
            file19 = []
            #This part calculate statstics for Client <---> Server direction
            for minpack in mincapture:

                file2.append(int(minpack['ip'].len))
                file3.append((minpack.sniff_time))
                if minpack['ip'].src == ipsource:
                    file4.append(int(minpack['ip'].len))
                    file5.append((minpack.sniff_time))
                elif minpack['ip'].dst == ipsource:
                    file6.append(int(minpack['ip'].len))
                    file7.append((minpack.sniff_time))


            for d in range(0, appdatalen):
                    if appdatalength[d].ip.src == ipsource:
                        file18.append(int(appdatalength[d].ssl.record_length))
                    elif appdatalength[d].ip.dst == ipsource:
                        file19.append(int(appdatalength[d].ssl.record_length))

            #########################################################

            saveToCSV_All_Direction(file2, file3, file4, file5, file6, file7, file18, file19, clientsni, outputfile)

# Function for packet size statistics
def PacketSizeCalculation(pktsize):
    result = str(math.ceil(len(pktsize))) + "," + str(math.ceil(numpy.mean(pktsize))) + "," + str(
        math.ceil(numpy.percentile(pktsize, 25))) + "," + str(math.ceil(numpy.percentile(pktsize, 50))) + "," + str(
        math.ceil(numpy.percentile(pktsize, 75))) + "," + str(math.ceil(numpy.var(pktsize))) + "," + str(
        math.ceil(numpy.max(pktsize)))

    del pktsize
    return result

# Function for Calculating the Inter Arrival Time and make statistics
def IATCalculcation(arrivaltime):
    counter = 1
    IAT = []

    for item in arrivaltime:
        if (counter != len(arrivaltime)):
            IAT.append((arrivaltime[counter] - arrivaltime[counter - 1]).total_seconds() * 1000)  #millesecond
            counter = counter + 1
    result = str(numpy.percentile(IAT, 25)) + "," + str(numpy.percentile(IAT, 50)) + "," + str(
        numpy.percentile(IAT, 75))
    del IAT
    return result


def AppdataStatistics(pktsize):
    if len(pktsize) == 0:
        pktsize.append(0)
    result = str(math.ceil(numpy.mean(pktsize))) + "," + str(math.ceil(numpy.percentile(pktsize, 25))) + "," + str(
        math.ceil(numpy.percentile(pktsize, 50))) + "," + str(math.ceil(numpy.percentile(pktsize, 75))) + "," + str(
        math.ceil(numpy.var(pktsize))) + "," + str(math.ceil(numpy.max(pktsize)))
    del pktsize
    return result


# Function used to store statistics 		
def saveToCSV_All_Direction(TCS_PSIZE, TCS_TIME, CS_PSIZE, CS_TIME, SC_PSIZE, SC_TIME, CS_appdata, SC_appdata, sni,
                            outputfile):

    file = open(outputfile, "a")
    file.write(sni + "," + PacketSizeCalculation(TCS_PSIZE)+ "," + IATCalculcation(TCS_TIME) + "," + PacketSizeCalculation(
            CS_PSIZE) + "," + IATCalculcation(CS_TIME) + "," + PacketSizeCalculation(SC_PSIZE) + "," + IATCalculcation(
            SC_TIME) + "," + AppdataStatistics(CS_appdata) + "," + AppdataStatistics(SC_appdata) + "\n")
    file.close()